package com.acme.banking.services;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

@Path("/customers")

public class CustomerService {
    @GET
    @Path("/new")
    public Response getAll() {
        String output = "1#customer1,2#customer2,3#customer3";
        return Response.status(200).entity(output).build();
    }

    @GET
    @Path("/{name}")
    public Response getCustomer(@PathParam("name") String name) {
        String output = "100#" + name;
        return Response.status(200).entity(output).build();

    }
}
